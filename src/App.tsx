import React from "react";
import { Provider } from "react-redux";

import Routes from "pages/Routes";
import store from "store";

import "./assets/styles/global.scss";

const App = () => {
  return (
    <Provider store={store}>
      <Routes />
    </Provider>
  );
};

export default App;

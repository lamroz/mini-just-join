import React from "react";

const Loader = ({ text = "Trwa ładowanie danych" }: { text?: string }) => {
  return <div className="w-full text-lg font-bold flex justify-center items-center height-without-header">{text}</div>;
};

export default Loader;

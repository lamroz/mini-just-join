import React from "react";
import { useHistory } from "react-router-dom";
import clsx from "clsx";

import Button from "../button/Button";

import classes from "./error.module.scss";

interface Props {
  status?: number | string;
  message?: string;
}

const ErrorSection = ({ status = 404, message = "Oferta nie została odnaleziona" }: Props) => {
  const history = useHistory();

  return (
    <div className="w-full flex flex-col justify-center items-center height-without-header">
      <div className={clsx("font-bold", classes.error_status)}>{status}</div>
      <div className="font-semibold text-lg mb-4 mt-8">{message}</div>

      <Button label="Wróć do poprzedniej strony" onClick={() => history.goBack()} />
    </div>
  );
};

export default ErrorSection;

import React from "react";
import { MapContainer as LeafletMapContainer, TileLayer } from "react-leaflet";
import { MapOptions } from "leaflet";

interface Props {
  style?: any;
  defaultCenter?: [number, number];
  defaultZoom?: number;
  className?: string;
  options?: MapOptions;
}

// Warsaw coordinates
const DEFAULT_LATITUDE = 52.229676;
const DEFAULT_LONGITUDE = 21.012229;

const MapContainer: React.FC<Props> = ({
  style = null,
  defaultCenter = [DEFAULT_LATITUDE, DEFAULT_LONGITUDE],
  defaultZoom = 7,
  className,
  options = {},
  children,
}) => {
  return (
    <LeafletMapContainer className={className} style={style} center={defaultCenter} zoom={defaultZoom} zoomControl={false} {...options}>
      <TileLayer url="https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png" />

      {children}
    </LeafletMapContainer>
  );
};

export default MapContainer;

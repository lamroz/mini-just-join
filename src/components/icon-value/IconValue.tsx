import React from "react";
import clsx from "clsx";

interface Props {
  icon: string;
  value: string | number;
}

const IconValue = ({ icon, value }: Props) => {
  return (
    <div className="flex items-center mr-4">
      <i className={clsx(icon, "text-gray-300 mr-2")} /> <span className="text-xs font-medium">{value}</span>
    </div>
  );
};

export default IconValue;

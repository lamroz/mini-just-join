import React from "react";
import clsx from "clsx";

import classes from "./button.module.scss";

interface Props {
  label: string;
  onClick: () => void;
  block?: boolean;
  className?: string;
}

const Button = ({ label, onClick, block, className }: Props) => {
  return (
    <button
      className={clsx("py-2 px-4 text-sm font-semibold shadow-md rounded", classes.button, { "w-full": block }, className)}
      onClick={onClick}
    >
      {label}
    </button>
  );
};

export default Button;

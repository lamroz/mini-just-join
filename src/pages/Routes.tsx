import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { Layout } from "layout";

import { OffersMainPage, OfferDetailsPage } from "./offers";

const Routes = () => {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route path="/" exact>
            <OffersMainPage />
          </Route>
          <Route path="/:id">
            <OfferDetailsPage />
          </Route>
        </Switch>
      </Layout>
    </Router>
  );
};

export default Routes;

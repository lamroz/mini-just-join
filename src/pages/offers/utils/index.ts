export enum TechnologiesEnum {
  JAVA = "java",
  JAVA_SCRIPT = "javascript",
  PHP = "php",
}

export const getTechnologyTypeIcon = (type: TechnologiesEnum) => {
  switch (type) {
    case TechnologiesEnum.JAVA_SCRIPT:
      return "fab fa-js";
    case TechnologiesEnum.JAVA:
      return "fab fa-java";
    case TechnologiesEnum.PHP:
      return "fab fa-php";
    default:
      return "fas fa-code";
  }
};

export const formatSalary = (salary_from: number, salary_to: number, salary_currency: string): string => {
  return `${salary_from} - ${salary_to} ${salary_currency?.toUpperCase()}`;
};

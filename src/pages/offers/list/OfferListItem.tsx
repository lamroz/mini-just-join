import React, { useMemo } from "react";
import { Link } from "react-router-dom";
import clsx from "clsx";

import { Offer } from "api/offers/model";
import { IconValue } from "components";

import SkillItem from "./SkillItem";

import classes from "./list.module.scss";
import { formatDate } from "utils/date-utils";
import { formatSalary } from "../utils";

interface Props {
  offer: Offer;
}

const OfferListItem = ({ offer }: Props) => {
  const { published_at, salary_from, salary_to, salary_currency, marker_icon } = offer;

  const formattedSalary = useMemo(() => {
    return formatSalary(salary_from, salary_to, salary_currency);
  }, [salary_from, salary_to, salary_currency]);

  const formattedDate = useMemo(() => {
    return formatDate(published_at);
  }, [published_at]);

  return (
    <Link to={`/${offer.id}`} className="select-none">
      <div className="w-full mb-4 p-4 flex shadow overflow-hidden bg-white rounded-lg hover:shadow-lg transition-all duration-300">
        <div className={clsx("mr-4 rounded-lg", classes.offer_type_indicator, classes[marker_icon])} />

        <div className="flex flex-col w-full">
          <div className="w-full flex justify-between text-sm font-semibold">
            {offer.company_name} <span className="text-gray-300">{formattedDate}</span>
          </div>
          <div className="font-extrabold text-xl mt-2 mb-1">{offer.title}</div>

          <div className="flex mb-4">
            <IconValue icon="far fa-credit-card" value={formattedSalary} />
            <IconValue icon="fas fa-briefcase" value={offer.employment_type} />
            <IconValue icon="fas fa-map-marker-alt" value={offer.city} />
          </div>

          <div className="w-full flex justify-end mt-2">
            {offer.skills.map(({ name }) => (
              <SkillItem key={name} skill={name} />
            ))}
          </div>
        </div>
      </div>
    </Link>
  );
};

export default OfferListItem;

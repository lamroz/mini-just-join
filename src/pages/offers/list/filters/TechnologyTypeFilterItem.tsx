import React from "react";
import clsx from "clsx";

import { TechnologiesEnum } from "pages/offers/utils";

import classes from "./filters.module.scss";

interface Props {
  selected: boolean;
  icon?: string;
  text?: string;
  onClick: () => void;
  type?: TechnologiesEnum;
}

const TechnologyTypeFilterItem = ({ selected, icon, onClick, type, text }: Props) => {
  return (
    <span
      className={clsx(
        "flex items-center justify-center ml-4 rounded-full cursor-pointer shadow-md select-none",
        classes.filter_item,
        type && classes[type],
        selected && (type ? classes[`selected_${type}`] : classes.unclassified_selected)
      )}
      onClick={onClick}
    >
      {text || <i className={clsx("fab text-2xl", icon)} />}
    </span>
  );
};

export default TechnologyTypeFilterItem;

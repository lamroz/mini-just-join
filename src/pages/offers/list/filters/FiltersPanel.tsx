import React from "react";
import clsx from "clsx";

import { useSearchQuery, useSearchQueryUpdate } from "hooks";

import { getTechnologyTypeIcon, TechnologiesEnum } from "../../utils";

import TechnologyTypeFilterItem from "./TechnologyTypeFilterItem";

import classes from "./filters.module.scss";

const offertTypeFilters: { icon: string; value: TechnologiesEnum }[] = [
  { icon: getTechnologyTypeIcon(TechnologiesEnum.JAVA), value: TechnologiesEnum.JAVA },
  { icon: getTechnologyTypeIcon(TechnologiesEnum.JAVA_SCRIPT), value: TechnologiesEnum.JAVA_SCRIPT },
  { icon: getTechnologyTypeIcon(TechnologiesEnum.PHP), value: TechnologiesEnum.PHP },
];

const OFFER_TYPE_QUERY_FILTER_KEY = "technology";

const FiltersPanel = () => {
  const { updateQuery, clearQuery } = useSearchQueryUpdate();

  const { technology } = useSearchQuery();

  return (
    <div className={clsx("w-full flex items-center justify-end", classes.filters_container)}>
      <TechnologyTypeFilterItem selected={!technology} onClick={clearQuery} text="All" />
      {offertTypeFilters.map(({ icon, value }) => (
        <TechnologyTypeFilterItem
          key={value}
          selected={!technology ? true : technology === value}
          icon={icon}
          onClick={() => updateQuery(OFFER_TYPE_QUERY_FILTER_KEY, value)}
          type={value}
        />
      ))}
    </div>
  );
};

export default FiltersPanel;

import React from "react";
import { useSelector } from "react-redux";
import clsx from "clsx";

import { useSearchQuery } from "hooks";

import { filteredOffersListSelector } from "../store/selectors";

import FiltersPanel from "./filters/FiltersPanel";
import OfferListItem from "./OfferListItem";

import classes from "./list.module.scss";

const OffersList = () => {
  const { technology } = useSearchQuery();

  const offersList = useSelector(filteredOffersListSelector(technology as string));

  return (
    <div className="w-full flex flex-col p-4">
      <FiltersPanel />

      <div className={clsx("px-2", classes.list_container)}>
        {offersList.map((offer) => (
          <OfferListItem key={offer.id} offer={offer} />
        ))}
      </div>
    </div>
  );
};

export default OffersList;

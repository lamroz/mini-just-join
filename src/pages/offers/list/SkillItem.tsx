import React from "react";
import clsx from "clsx";

import classes from "./list.module.scss";

const SkillItem = ({ skill }: { skill: string }) => {
  return <span className={clsx("text-xs font-semibold shadow-md rounded-md px-2 py-1 ml-2", classes.skill_item)}>{skill}</span>;
};

export default SkillItem;

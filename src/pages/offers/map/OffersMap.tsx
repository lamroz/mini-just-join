import React from "react";
import clsx from "clsx";

import { MapContainer } from "components";

import OffersMarkers from "./OffersMarkers";

import classes from "./map.module.scss";

const OffersMap: React.FC = ({ children }) => {
  return (
    <div className="w-full h-full relative">
      <MapContainer className={classes.map_container}>
        <OffersMarkers />
      </MapContainer>

      <div className={clsx("w-full h-full relative flex justify-end", classes.children_content)}>
        <div className="flex justify-end">{children}</div>
      </div>
    </div>
  );
};

export default OffersMap;

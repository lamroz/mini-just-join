import React, { Fragment } from "react";
import { useSelector } from "react-redux";

import OfferMarker from "./OfferMarker";
import { filteredOffersListSelector } from "../store/selectors";
import { useSearchQuery } from "hooks";

const OffersMarkers = () => {
  const { technology } = useSearchQuery();

  const offersList = useSelector(filteredOffersListSelector(technology as string));

  return (
    <Fragment>
      {offersList.map(({ id, latitude, longitude, company_name, title, marker_icon }) => (
        <OfferMarker
          key={id}
          latitude={latitude}
          longitude={longitude}
          company_name={company_name}
          title={title}
          marker_icon={marker_icon}
          id={id}
        />
      ))}
    </Fragment>
  );
};

export default OffersMarkers;

import L, { DivIcon } from "leaflet";

import { getTechnologyTypeIcon, TechnologiesEnum } from "../utils";

export const getTechnologyMarkerIcon = (type: TechnologiesEnum): DivIcon => {
  return L.divIcon({
    className: `technology-icon ${type}`,
    html: `<i class="fas fa-map-marker"><i class="${getTechnologyTypeIcon(type)}"></i></i>`,
  });
};

import React, { memo } from "react";
import { Marker, Popup } from "react-leaflet";
import { useHistory } from "react-router-dom";

import { Offer } from "api/offers/model";
import { Button } from "components";

import { getTechnologyMarkerIcon } from "./map.utils";
import { TechnologiesEnum } from "../utils";

interface Props extends Pick<Offer, "latitude" | "longitude" | "company_name" | "title" | "marker_icon"> {
  id?: string;
}

const OfferMarker = ({ latitude, longitude, company_name, title, marker_icon, id }: Props) => {
  const history = useHistory();

  return (
    <Marker position={[latitude, longitude]} icon={getTechnologyMarkerIcon(marker_icon as TechnologiesEnum)}>
      <Popup>
        <div className="font-semibold text-xs mb-2">{company_name}</div>
        <div className="font-extrabold text-base">{title}</div>

        {id && <Button label="Szczegóły" className="my-2" block onClick={() => history.push(`/${id}`)} />}
      </Popup>
    </Marker>
  );
};

export default memo(OfferMarker);

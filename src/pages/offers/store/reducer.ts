import { Offer } from "api/offers/model";

import { OffersActionTypes, SET_OFFERS_LIST } from "./types";

export interface OffersState {
  list: Offer[];
}

const initialState: OffersState = {
  list: [],
};

const offersReducer = (state = initialState, action: OffersActionTypes): OffersState => {
  switch (action.type) {
    case SET_OFFERS_LIST: {
      return {
        ...state,
        list: action.payload,
      };
    }
    default:
      return state;
  }
};

export default offersReducer;

import { createSelector } from "reselect";

import { RootState } from "store/reducers";

const offersListSelector = (state: RootState) => state.offers.list;

export const filteredOffersListSelector = (technologyType?: string) =>
  createSelector(offersListSelector, (offersList) => {
    if (technologyType) {
      return offersList.filter(({ marker_icon }) => marker_icon === technologyType);
    }

    return offersList;
  });

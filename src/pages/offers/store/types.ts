import { Offer } from "api/offers/model";

export const SET_OFFERS_LIST = "[OFFERS] SET_OFFERS_LIST";

interface SetOffersListAction {
  type: typeof SET_OFFERS_LIST;
  payload: Offer[];
}

export type OffersActionTypes = SetOffersListAction;

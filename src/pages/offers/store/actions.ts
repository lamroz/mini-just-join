import API from "api";
import { Action } from "redux";
import { ThunkAction } from "redux-thunk";
import { AxiosError } from "axios";

import { Offer } from "api/offers/model";
import { RootState } from "store/reducers";

import { OffersActionTypes, SET_OFFERS_LIST } from "./types";

const setOffersList = (offers: Offer[]): OffersActionTypes => ({
  type: SET_OFFERS_LIST,
  payload: offers,
});

export const getOffersList = (): ThunkAction<void, RootState, unknown, Action<string>> => (dispatch) => {
  API.offers
    .getOffersListUsingGET()
    .then((response) => dispatch(setOffersList(response.data)))
    .catch((error: AxiosError) => console.error(error));
};

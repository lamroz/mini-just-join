import React from "react";

import { Skill } from "api/offers/model";

const SKILL_LEVELS = [1, 2, 3, 4, 5];

const SkillWithLevelItem = ({ name, level }: Skill) => {
  return (
    <div className="text-center">
      <div className="font-semibold">{name}</div>
      {SKILL_LEVELS.map((skillLevel) => {
        return skillLevel <= level ? (
          <i className="fas fa-square text-sm text-primary mr-1" />
        ) : (
          <i className="far fa-square text-gray-300 text-sm mr-1" />
        );
      })}
    </div>
  );
};

export default SkillWithLevelItem;

import React, { useMemo } from "react";
import clsx from "clsx";

import { MapContainer } from "components";
import { OfferDetails } from "api/offers/model";

import classes from "./details.module.scss";
import OfferMarker from "../map/OfferMarker";

const OfferMapSection: React.FC<Pick<OfferDetails, "latitude" | "longitude" | "company_name" | "title" | "marker_icon">> = (props) => {
  const { title, company_name, latitude, longitude, marker_icon, children } = props;

  const coordinates: [number, number] = useMemo(() => {
    return [Number(latitude), Number(longitude)];
  }, [latitude, longitude]);

  return (
    <div className="w-full h-full relative">
      <MapContainer
        className={classes.map_container}
        defaultCenter={coordinates}
        defaultZoom={12}
        options={{ scrollWheelZoom: false, dragging: false }}
      >
        <OfferMarker
          latitude={Number(latitude)}
          longitude={Number(longitude)}
          title={title}
          company_name={company_name}
          marker_icon={marker_icon}
        />
      </MapContainer>

      <div className={clsx("w-full h-full relative flex justify-end", classes.children_content)}>
        <div className="flex justify-end">{children}</div>
      </div>
    </div>
  );
};

export default OfferMapSection;

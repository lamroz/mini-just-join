import React, { useCallback } from "react";
import { useHistory } from "react-router-dom";
import clsx from "clsx";

import { OfferDetails } from "api/offers/model";
import { Button, IconValue } from "components";

import { formatSalary } from "../utils";

import SkillWithLevelItem from "./SkillWithLevelItem";

import classes from "./details.module.scss";

interface Props {
  offer: OfferDetails;
}

const OfferDescriptionSection = ({ offer }: Props) => {
  const history = useHistory();

  const handleApplicationSend = useCallback(() => {
    // TODO: do something here
  }, []);

  return (
    <div className={clsx("w-full flex flex-col p-4 pt-0", classes.offer_description_container)}>
      <div className="h-10 my-6 flex items-center justify-between">
        <i
          className="fas fa-arrow-left text-gray-400 hover:text-gray-600 text-lg cursor-pointer transition-colors"
          onClick={() => history.goBack()}
        />

        <Button label="Aplikuj" onClick={handleApplicationSend} />
      </div>

      <div className={clsx("p-4 shadow overflow-hidden bg-white rounded-lg", classes.list_container)}>
        <div className="flex flex-col w-full">
          <div className={clsx("rounded-lg mb-4", classes.offer_type_indicator)} />

          <div className="relative mb-4">
            <div className="font-body text-sm">{offer.company_name}</div>
            <div className="font-extrabold text-2xl">{offer.title}</div>

            <a href={offer.company_url} target="_blank" rel="noreferrer">
              <img className={clsx("absolute w-32", classes.company_logo)} src={offer.company_logo_url} alt={offer.company_name} />
            </a>
          </div>

          <div className="flex mb-8">
            {offer.employment_types.map(({ type, salary }) => {
              return (
                <IconValue
                  key={type}
                  icon="far fa-credit-card"
                  value={`${type} | ${formatSalary(salary.from, salary.to, salary.currency)}`}
                />
              );
            })}
            <IconValue icon="fas fa-map-marker-alt" value={offer.address_text} />
            {offer.remote && <IconValue icon="fas fa-house-user" value="Praca zdalna" />}
            {offer.remote_interview && <IconValue icon="fas fa-phone-volume" value="Rekrutacja online" />}
          </div>

          <div className="flex justify-between mb-8">
            {offer.skills.map(({ name, level }, index) => (
              <SkillWithLevelItem key={index} name={name} level={level} />
            ))}
          </div>

          <div className="text-sm" dangerouslySetInnerHTML={{ __html: offer.body }} />

          <div className="my-8">
            <Button label="Aplikuj" onClick={handleApplicationSend} block />
          </div>

          {offer.information_clause && (
            <div className="text-xs text-gray-300" dangerouslySetInnerHTML={{ __html: offer.information_clause }} />
          )}
        </div>
      </div>
    </div>
  );
};

export default OfferDescriptionSection;

import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import API from "api/index";
import { OfferDetails } from "api/offers/model";
import { Loader, ErrorSection } from "components";

import OfferDescriptionSection from "./OfferDescriptionSection";
import OfferMapSection from "./OfferMapSection";

const OfferDetailsPage = () => {
  const { id } = useParams<{ id: string }>();

  const [offerDetails, setOfferDetails] = useState<OfferDetails>((null as unknown) as OfferDetails);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    API.offers
      .getOfferDetailsUsingGET(id)
      .then((response) => setOfferDetails(response))
      .finally(() => setLoading(false));
  }, [id]);

  if (loading) {
    return <Loader />;
  }

  if (!loading && !offerDetails) {
    return <ErrorSection />;
  }

  return (
    <div className="flex">
      <OfferMapSection
        title={offerDetails.title}
        company_name={offerDetails.company_name}
        latitude={offerDetails.latitude}
        longitude={offerDetails.longitude}
        marker_icon={offerDetails.marker_icon}
      >
        <OfferDescriptionSection offer={offerDetails} />
      </OfferMapSection>
    </div>
  );
};

export default OfferDetailsPage;

import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

import { getOffersList } from "./store/actions";

import OffersList from "./list/OffersList";
import OffersMap from "./map/OffersMap";

const OffersMainPage = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getOffersList());
  }, [dispatch]);

  return (
    <div className="flex">
      <OffersMap>
        <OffersList />
      </OffersMap>
    </div>
  );
};

export default OffersMainPage;

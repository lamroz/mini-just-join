import React from "react";
import { Link } from "react-router-dom";
import clsx from "clsx";

import classes from "./header.module.scss";

const Header = () => {
  return (
    <div className={clsx("w-full px-16 flex items-center justify-center shadow-lg relative", classes.header)}>
      <div className="text-xl font-black max-w-screen-xl w-full flex justify-between">
        <Link to="/">
          <span>Mini </span>JustJoinIT
        </Link>

        <i className="fas fa-user-circle text-3xl text-gray-300 hover:text-current cursor-pointer transition-colors" />
      </div>
    </div>
  );
};

export default Header;

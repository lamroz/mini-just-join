import React from "react";

import Header from "./header/Header";

const Layout: React.FC = ({ children }) => {
  return (
    <div className="w-full">
      <Header />

      {children}
    </div>
  );
};

export default Layout;

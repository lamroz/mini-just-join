import { useMemo } from "react";
import { useLocation } from "react-router-dom";
import qs from "qs";

const useSearchQuery = () => {
  const location = useLocation();

  return useMemo(() => {
    return qs.parse(location.search, { ignoreQueryPrefix: true });
  }, [location.search]);
};

export default useSearchQuery;

import { useCallback } from "react";
import { useHistory, useLocation } from "react-router-dom";
import useSearchQuery from "./useSearchQuery";
import qs from "qs";

const useSearchQueryUpdate = () => {
  const history = useHistory();
  const location = useLocation();

  const queryParams = useSearchQuery();

  const updateQuery = useCallback(
    (key: string, value: string) => {
      const newSearchQuery = { ...queryParams };

      const queryKeyValue = queryParams[key];

      if (typeof queryKeyValue === "string") {
        if (queryKeyValue === value) {
          delete newSearchQuery[key];
        } else {
          newSearchQuery[key] = value;
        }
      }

      if (!queryKeyValue) {
        newSearchQuery[key] = value;
      }

      history.push({ pathname: location.pathname, search: qs.stringify(newSearchQuery) });
    },
    [history, location.pathname, queryParams]
  );

  const clearQuery = useCallback(() => {
    history.push({ pathname: location.pathname });
  }, [history, location.pathname]);

  return {
    clearQuery,
    updateQuery,
  };
};

export default useSearchQueryUpdate;

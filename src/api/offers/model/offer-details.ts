import { Offer } from "./offer";
import { EmploymentType } from "./employment-type";

type Exlude = "employment_type" | "salary_currency" | "salary_from" | "salary_to" | "latitude" | "longitude";

export interface OfferDetails extends Omit<Offer, Exlude> {
  address_text: string;
  apply_body: string;
  apply_url: string;
  banner_url: string;
  body: string;
  custom_consent: string;
  custom_consent_title: string;
  employment_types: EmploymentType[];
  future_consent: string;
  future_consent_title: string;
  information_clause: string;
  latitude: string;
  longitude: string;
  remote_interview: boolean;
  tags: string[];
  video_key: string;
  video_provider: string;
  workplace_type: string;
}

export interface EmploymentType {
  salary: {
    from: number;
    to: number;
    currency: string;
  };
  type: string;
}

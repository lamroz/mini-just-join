import axios, { AxiosError } from "axios";

import { Offer, OfferDetails } from "./offers/model";

import offerDetails from "./offer-details.json";

const axiosInstance = axios.create({
  baseURL: "https://test.justjoin.it",
});

const API = {
  offers: {
    getOffersListUsingGET: () => axiosInstance.get<Offer[]>("/offers"),
    getOfferDetailsUsingGET: (id: string) =>
      // TODO: Po naprawieniu enpointu zwracającego szczegóły, należy zostawić samo wywołanie `/offers/:id`
      // Wrapper z Promisa dodany wyłącznie w celu zamockowania danych oferty w przypadku błędu
      new Promise<OfferDetails>((resolve) => {
        axiosInstance
          .get(`/offers/${id}`)
          .then((response) => resolve(response.data))
          .catch((error: AxiosError) => {
            console.error(error);
            resolve((offerDetails as unknown) as OfferDetails);
          });
      }),
  },
};

export default API;

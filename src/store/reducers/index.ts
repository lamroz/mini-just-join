import { combineReducers } from "redux";

import offersReducer from "../../pages/offers/store/reducer";

const rootReducer = combineReducers({
  offers: offersReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
